FROM node:lts-buster
WORKDIR /ragdoc-view
ADD . /ragdoc-view/
RUN npm install
RUN npm install -g @angular/cli@11.1.1
VOLUME ["/data"]
VOLUME ["/dist"]
RUN chmod +x build-view.sh
ENV BASE_HREF="."
ENTRYPOINT [""]
