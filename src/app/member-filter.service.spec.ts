import { TestBed, inject } from '@angular/core/testing';

import { MemberFilterService } from './member-filter.service';

describe('MemberFilterService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [MemberFilterService]
    });
  });

  it('should ...', inject([MemberFilterService], (service: MemberFilterService) => {
    expect(service).toBeTruthy();
  }));
});
