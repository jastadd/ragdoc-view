import {AstDecl} from './ast-decl/ast-decl';

export class Doc {
  ast: string;
  astdecl: AstDecl;
  ragFile: string;
  line: number;
  description: string;
  apilevel: string;
  relation: string;
  params: string[]

  static fromJson(json: any): Doc {
    var astdecl: AstDecl = undefined;
    if (json.astdecl) {
      astdecl = AstDecl.fromJson(json.astdecl);
    }
    return {
      ast: json.ast,
      astdecl: astdecl,
      ragFile: json.ragFile,
      line: json.line,
      description: json.description,
      apilevel: json.apilevel,
      relation: json.relation,
      params: json.params,
    };
  }
}
