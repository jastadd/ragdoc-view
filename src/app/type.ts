import {Doc} from './doc';
import {Member} from './member';
import {TypeRef} from './type-ref';
import {InheritedMembers} from './inherited-members';

export class Type {
  id: string;
  kind: string;
  name: string;
  pkg: string;
  mods: string[];
  doc: Doc;
  groups: { [id: string] : Member[]; };
  args: TypeRef[]; // Type arguments.
  superclass: TypeRef;
  superinterfaces: TypeRef[];
  inherited_methods: InheritedMembers[];
  inherited_relations: InheritedMembers[];
  inherited_attributes: InheritedMembers[];
  inherited_fields: InheritedMembers[];
  subtypes: TypeRef[];

  static fromJson(json: any): Type {
    var doc: Doc = undefined;
    if (json.doc) {
      doc = Doc.fromJson(json.doc);
    }
    var groups = {};
    if (json.groups) {
      for (var i = 0; i < json.groups.length; i++) {
        var group = json.groups[i];
        if (group.members) {
          groups[group.kind] = (group.members as Member[]).map(member => Member.fromJson(member));
        }
      }
    }
    var superclass: TypeRef = undefined;
    if (json.superclass) {
      superclass = TypeRef.fromJson(json.superclass);
    }
    var superinterfaces: TypeRef[] = [];
    if (json.superinterfaces) {
      superinterfaces = (json.superinterfaces as TypeRef[]).map(TypeRef.fromJson);
    }
    var inherited_methods: InheritedMembers[] = undefined;
    if (json.inherited_methods) {
      inherited_methods = (json.inherited_methods as any[]).map(inherited =>
          InheritedMembers.fromJson(inherited));
    }
    var inherited_relations: InheritedMembers[] = undefined;
    if (json.inherited_relations) {
      inherited_relations = (json.inherited_relations as any[]).map(inherited =>
          InheritedMembers.fromJson(inherited));
    }
    var inherited_attributes: InheritedMembers[] = undefined;
    if (json.inherited_attributes) {
      inherited_attributes = (json.inherited_attributes as any[]).map(inherited =>
          InheritedMembers.fromJson(inherited));
    }
    var inherited_fields: InheritedMembers[] = undefined;
    if (json.inherited_fields) {
      inherited_fields = (json.inherited_fields as any[]).map(inherited =>
          InheritedMembers.fromJson(inherited));
    }
    var args: TypeRef[] = undefined;
    if (json.args) {
      args = (json.args as any[]).map(arg => TypeRef.fromJson(arg));
    }
    var subtypes: TypeRef[] = undefined;
    if (json.subtypes) {
      subtypes = (json.subtypes as any[]).map(arg => TypeRef.fromJson(arg));
    }
    return {
      id: TypeRef.typeId(json.name, json.id),
      kind: json.kind,
      name: json.name,
      pkg: json.pkg,
      mods: json.mods as string[],
      doc: doc,
      groups: groups,
      args: args,
      superclass: superclass,
      superinterfaces: superinterfaces,
      inherited_methods: inherited_methods,
      inherited_relations: inherited_relations,
      inherited_attributes: inherited_attributes,
      inherited_fields: inherited_fields,
      subtypes: subtypes,
    };
  }
}

