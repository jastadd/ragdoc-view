import { NgModule }      from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { RouterModule } from '@angular/router';

import { AppComponent }  from './app.component';
import { TypeDetailsComponent }  from './type-details.component';
import { ParametersComponent }  from './parameters.component';
import { TypeReferenceComponent } from './type-ref.component';
import { NameFilterPipe } from './name-filter.pipe';
import { StringFilterPipe } from './string-filter.pipe';
import { SourceViewComponent } from './source-view/source-view.component';
import { EditorDirective } from './editor.directive';
import { DeclaredAtComponent } from './declared-at/declared-at.component';
import { AstDeclComponent } from './ast-decl/ast-decl.component';

@NgModule({
  imports:      [
    BrowserModule,
    FormsModule,
    HttpModule,
    RouterModule.forRoot([
      {
        path: 'type/:id',
        component: TypeDetailsComponent
      },
      {
        path: 'source/:filename/:line',
        component: SourceViewComponent
      }
    ], { useHash: true }),
  ],
  declarations: [
    AppComponent,
    TypeDetailsComponent,
    ParametersComponent,
    TypeReferenceComponent,
    NameFilterPipe,
    StringFilterPipe,
    SourceViewComponent,
    EditorDirective,
    DeclaredAtComponent,
    AstDeclComponent,
  ],
  bootstrap:    [ AppComponent ]
})
export class AppModule { }
