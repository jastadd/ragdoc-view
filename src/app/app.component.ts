import { Component } from '@angular/core';
import { OnInit } from '@angular/core';

import { Package } from './package';
import { Type } from './type';
import { PackageService } from './package.service';
import { SelectionService } from './selection.service';
import { Member } from './member';
import { Parameter } from './parameter';

import { ActivatedRoute, Params } from '@angular/router';
import { Location } from '@angular/common';

@Component({
  selector: 'app-root',
  styleUrls: ['./app.component.css'],
  template: `
<nav class="topnav">
  <img src="assets/hamburger_24px.svg" (click)="showMenu = !showMenu"><h1>{{title}}</h1>
</nav>

<nav class="sidenav" [class.hidemenu]="!showMenu">
  <div class="search"><input [(ngModel)]="filter" placeholder="Search..."></div>

  <ng-template ngFor let-package [ngForOf]="packages">
    <div *ngIf="filteredPackage(package)">
      <div class="package"><b>{{package.name}}</b></div>
      <ng-template ngFor let-group [ngForOf]="package.groups">
        <div *ngIf="filteredGroup(group)">
          <div class="group" (click)="group.hidden = !group.hidden">{{typeKindNames[group.kind]}}<img src="assets/eye_24px.svg" *ngIf="!group.hidden"><img src="assets/eye_off_24px.svg" *ngIf="group.hidden"></div>
          <ul class="types" [hidden]="group.hidden">
          <li *ngFor="let type of group.members | nameFilter:filter" class="type"
            [class.selected]="type.id === selectedType"
            [routerLink]="['/type', type.id]"
            (click)="showMenu = false">
            {{type.name}}
          </li>
          </ul>
        </div>
      </ng-template>
    </div>
  </ng-template>
</nav>

<article>
    <router-outlet></router-outlet>
</article>
  `,
  providers: [
    PackageService,
    SelectionService,
  ],
})
export class AppComponent implements OnInit {
  title = 'Documentation';
  showMenu = false;
  packages : Package[];
  filter = '';
  selectedType = '';

  private typeKindNames = {
    'ast-class': 'AST CLASSES',
    'interface': 'INTERFACES',
    'class': 'CLASSES',
  };

  constructor(private packageService: PackageService,
      private selectionService: SelectionService) {
    selectionService.selection$.subscribe(id => this.selectedType = id);
  }

  ngOnInit(): void {
    this.packageService.getPackages().then(packages => this.packages = packages);
  }

  filteredPackage(pkg: Package): boolean {
    var filter = this.filter.toLowerCase();
    for (var i = 0; i < pkg.groups.length; i++) {
      if (this.filteredGroup(pkg.groups[i])) {
        return true;
      }
    }
    return false;
  }

  filteredGroup(group: any): boolean {
    var filter = this.filter.toLowerCase();
    var filtered = group.members.filter(member => member.name.toLowerCase().indexOf(filter) >= 0);
    return filtered.length > 0;
  }
}

