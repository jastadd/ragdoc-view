import { PackageEntry } from './package-entry';

export class Package {
  name: string;
  groups: any[];

  static fromJson(json: any): Package {
    return json as Package;
  }
}


